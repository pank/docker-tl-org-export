# docker-org-texlive

Docker image providing a [snapshot verison of emacs](http://emacs.secretsauce.net), [TeXLive](https://tug.org/texlive/) (via [TinyTex](https://yihui.org/tinytex/)) and [Pandoc](https://pandoc.org). Recent versions of [org-mode](orgmode.org/), [citeproc](https://github.com/andras-simonyi/citeproc-el) and [ox-pandoc](https://github.com/emacsorphanage/ox-pandoc/) are also included. 
