FROM debian:unstable-slim
LABEL maintainer "Rasmus <docker@pank.eu>"
LABEL version="0.9.6"

ENV DEBIAN_FRONTEND="noninteractive"
ENV EMACS_SNAPSHOT_REPO="deb [arch=amd64, signed-by=/usr/share/keyrings/secretsauce-archive-keyring.gpg] http://emacs.secretsauce.net unstable main"

COPY setup.el /
COPY latex-packages.txt /

# Install Emacs, TeX and friends for Org-mode
RUN  apt-get update                                             \
    &&  apt-get install --yes --no-install-recommends apt-utils \
    && apt-get install --yes --no-install-recommends            \
    software-properties-common                                  \ 
    make                                                        \
    wget                                                        \
    ca-certificates                                             \
    grep                                                        \
    fontconfig                                                  \
    perl                                                        \
    python-is-python3                                           \
    python3-pygments                                            \
    gnupg                                                       \
    git                                                         \
    latexdiff                                                   \
    patch

# Install latest Emacs snapshot
RUN wget -qO emacs.gpg http://emacs.secretsauce.net/key.gpg                            \
    && gpg -q --no-default-keyring --keyring ./emacs-keyring.gpg --import emacs.gpg    \
    && gpg -q --no-default-keyring --keyring ./emacs-keyring.gpg --export              \
    --output /usr/share/keyrings/secretsauce-archive-keyring.gpg                       \
    && echo ${EMACS_SNAPSHOT_REPO} >> /etc/apt/sources.list                            \
    && apt-get update                                                                  \
    && apt-get install --yes --no-install-recommends emacs-snapshot-nox \
    # Install Org and friends
    && emacs --batch --no-init-file --load setup.el

# Install latest pandoc
Run wget -qO pandoc.deb $(wget -q https://api.github.com/repos/jgm/pandoc/releases/latest -O - | grep -E 'https://.*amd64.deb' -o) \ 
    && dpkg -i pandoc.deb                                                                                                                \
    && rm pandoc.deb             

# Install TinyTeX
RUN wget -qO- "https://yihui.org/tinytex/install-unx.sh" | sh -s - --admin --no-path                \
    && ~/.TinyTeX/bin/*/tlmgr install `cat latex-packages.txt` || true                              \
    && ~/.TinyTeX/bin/*/tlmgr option sys_bin /usr/bin/                                              \
    && ~/.TinyTeX/bin/*/tlmgr path add                                                              \
    && cp ~/.TinyTeX/texmf-var/fonts/conf/texlive-fontconfig.conf /etc/fonts/conf.d/09-texlive.conf \
    && fc-cache -fsv

# Clean up 
RUN apt-get autoclean                      \
    && apt-get remove --yes gpg            \
    && apt-get --purge --yes autoremove    \
    && apt-get clean                       \
    && rm -rf /var/lib/apt/lists/*         \
    && rm -rf /var/cache/*                 \
    && rm -rf /tmp/*                       \
    && rm -rf /var/tmp/*                   \
    && rm -rf /usr/share/doc               \
    && rm -rf /usr/share/man               \
    && rm -rf /usr/share/locale            \
    && rm ~/.TinyTeX/texmf-var/web2c/*.log 

WORKDIR /data
VOLUME ["/data"]
